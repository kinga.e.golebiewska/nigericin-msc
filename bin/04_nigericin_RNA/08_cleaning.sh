#!/bin/bash
read my_samples < samples.txt
read proj_name  < project_name.txt

my_dir3=../../raw_data/star/$proj_name/counts
rm $my_dir3/*.mate1

my_dir3=../../raw_data/star/$proj_name
rm $my_dir3/*.mate1