#!/bin/bash

read my_samples < samples.txt
read proj_name  < project_name.txt

my_dir=../../raw_data/trim_galore/$proj_name/trim_1
mkdir -p $my_dir
my_dir2=../../raw_data/trim_galore/$proj_name/trim_2
mkdir -p $my_dir2

for i in $my_samples
do

# 1. Adapter trimming
my_file=../../raw_data/reads/$proj_name/not_trimmed/${i}.fastq.gz
trim_galore -j 4 $my_file --fastqc --length 18 \
	-a TGGAATTCTCGGGTCCAAGG \
	-o $my_dir
	
# 2. removal of duplicated reads (PCR duplicates) using SeqKit and rmdup function
my_file=$my_dir/${i}_trimmed.fq.gz
tput setaf 1; echo "\nThe results of PCR duplicates removal:"
zcat $my_file | seqkit rmdup -s \
	-o $my_dir/${i}_rmdup.fq.gz
tput setaf 1; echo "======================================\n"
sleep 5
tput sgr0
	
# 3. UMIs removal using trim_galore
my_file=$my_dir/${i}_rmdup.fq.gz
trim_galore -j 4 $my_file --fastqc --length 18 \
	-a X \
	--clip_R1 4 --three_prime_clip_R1 4 \
	-o $my_dir2
	
# 4. unzip gz trimmed file
my_file=$my_dir2/${i}_rmdup_trimmed.fq.gz
gzip -d $my_file
	
done

