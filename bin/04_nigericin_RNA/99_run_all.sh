#!/bin/bash

sh 01_trimming.sh
sh 02_mapping_Pt.sh
sh 03_mapping_Mt.sh
sh 04_mapping_all.sh

sh 05_mapping_Pt_counts.sh
sh 06_mapping_Mt_counts.sh
sh 07_mapping_all_counts.sh
sh 08_cleaning.sh

tput setaf 2; echo "FINISHED!"; tput sgr0
