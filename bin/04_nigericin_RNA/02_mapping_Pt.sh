#!/bin/bash
read my_samples < samples.txt
read proj_name  < project_name.txt

my_dir2=../../raw_data/trim_galore/$proj_name/trim_2
my_dir3=../../raw_data/star/$proj_name
mkdir -p $my_dir3

for i in $my_samples
do
my_file=$my_dir2/${i}_rmdup_trimmed.fq
echo "========================="
tput setaf 1; echo "Mapping reads from $my_file"; tput sgr0

STAR --runThreadN 12 \
	--genomeDir /bin/STAR-2.7.6a/genomes/Arabidopsis_thaliana.TAIR10.dna.Pt \
	--readFilesIn $my_file \
	--outFilterMismatchNmax 2 \
	--outMultimapperOrder Random --outSAMmultNmax 1 \
	--alignIntronMax 1000 \
	--outFilterMatchNmin 18 \
	--outSAMunmapped Within --outReadsUnmapped Fastx \
	--limitBAMsortRAM 1100000000 \
	--outFileNamePrefix $my_dir3/${i}_Pt_ \
	--outSAMtype BAM SortedByCoordinate
done