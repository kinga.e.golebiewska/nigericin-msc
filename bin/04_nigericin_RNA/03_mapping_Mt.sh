#!/bin/bash
read my_samples < samples.txt
read proj_name  < project_name.txt

my_dir3=../../raw_data/star/$proj_name

for i in $my_samples
do
my_file=$my_dir3/${i}_Pt_Unmapped.out.mate1

echo "========================="
tput setaf 1; echo "Mapping reads from $my_file"; tput sgr0

STAR --runThreadN 12 \
	--genomeDir /bin/STAR-2.7.6a/genomes/Arabidopsis_thaliana.TAIR10.dna.Mt \
	--readFilesIn $my_file \
	--outFilterMismatchNmax 2 \
	--outMultimapperOrder Random --outSAMmultNmax 1 \
	--outFilterMatchNmin 18 \
	--outSAMunmapped Within --outReadsUnmapped Fastx \
	--outFileNamePrefix $my_dir3/${i}_Mt_ \
	--limitBAMsortRAM 1100000000 \
	--outSAMtype BAM SortedByCoordinate
done