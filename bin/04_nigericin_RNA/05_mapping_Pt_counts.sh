#!/bin/bash
read my_samples < samples.txt
read proj_name  < project_name.txt

my_dir2=../../raw_data/trim_galore/$proj_name/trim_2
my_dir3=../../raw_data/star/$proj_name/counts
mkdir -p $my_dir3


## 1. mapping to Pt noncoding RNAs
#echo "Mapping to chloroplast ncRNAs"
#for i in $my_samples
#do
#my_file=$my_dir2/${i}_rmdup_trimmed.fq
#echo "========================="
#tput setaf 1; echo "Mapping reads from $my_file"; tput sgr0

#STAR --runThreadN 12 \
	--genomeDir /bin/STAR-2.7.6a/genomes/Arabidopsis_thaliana.TAIR10.ncrna.Pt \
	--readFilesIn $my_file \
	--outFilterMismatchNmax 2 \
	--outMultimapperOrder Random --outSAMmultNmax 1 \
	--alignIntronMax 1 --alignIntronMin 2 \
	--outFilterMatchNmin 18 \
	--outSAMunmapped Within --outReadsUnmapped Fastx \
	--outFileNamePrefix $my_dir3/${i}_ncrna.Pt_  \
	--limitBAMsortRAM 1100000000 \
	--outSAMtype BAM SortedByCoordinate
#done

## 2. mapping to Pt mRNAs
#echo "Mapping to chloroplast mRNAs"
#for i in $my_samples
#do
#my_file=$my_dir3/${i}_ncrna.Pt_Unmapped.out.mate1
#echo "========================="
#tput setaf 1; echo "Mapping reads from $my_file"; tput sgr0

#STAR --runThreadN 12 \
	--genomeDir /bin/STAR-2.7.6a/genomes/Arabidopsis_thaliana_cp_transcripts \
	--readFilesIn $my_file \
	--outFilterMismatchNmax 2 \
	--outMultimapperOrder Random --outSAMmultNmax 1 \
	--alignIntronMax 1 --alignIntronMin 2 \
	--outFilterMatchNmin 18 \
	--outSAMunmapped Within --outReadsUnmapped Fastx \
	--limitBAMsortRAM 1100000000 \
	--outFileNamePrefix $my_dir3/${i}_mRNA.Pt_ \
	--outSAMtype BAM SortedByCoordinate
#done

## 3. mapping to Pt genome
echo "Mapping to chloroplast genome"
for i in $my_samples
do
my_file=$my_dir3/${i}_mRNA.Pt_Unmapped.out.mate1
echo "========================="
tput setaf 1; echo "Mapping reads from $my_file"; tput sgr0

STAR --runThreadN 12 \
	--genomeDir /bin/STAR-2.7.6a/genomes/Arabidopsis_thaliana.TAIR10.dna.Pt \
	--readFilesIn $my_file \
	--outFilterMismatchNmax 2 \
	--outMultimapperOrder Random --outSAMmultNmax 1 \
	--alignIntronMax 1000 \
	--outFilterMatchNmin 18 \
	--outSAMunmapped Within --outReadsUnmapped Fastx \
	--limitBAMsortRAM 1100000000 \
	--outFileNamePrefix $my_dir3/${i}_gDNA.Pt_ \
	--outSAMtype BAM SortedByCoordinate
done
