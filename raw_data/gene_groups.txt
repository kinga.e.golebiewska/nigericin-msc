TAIR	AGI	group
PIF1	AT2G20180	light
PIF3	AT1G09530	light
PIF4	AT2G43010	light
PIF5	AT3G59060	light
PIF7	AT5G61270	light
PHYA	AT1G09570	light
PHYB	AT2G18790	light
PHYC	AT5G35840	light
PHYD	AT4G16250	light
PHYE	AT4G18130	light
CRY1	AT4G08920	light
CRY2	AT1G04400	light
PHOT1	AT3G45780	light
PHOT2	AT5G58140	light
ZTL	AT5G57360	light
FKF1	AT1G68050	light
LKP2	AT2G18915	light
UVR8	AT5G63860	light
ELIP1	AT3G22840	light
ELIP2	AT4G14690	light
NCED9	AT1G78390	ABA
NCED5	AT1G30100	ABA
NCED3	AT3G14440	ABA
CYP707A4	AT3G19270	ABA
CYP707A3	AT5G45340	ABA
CYP707A1	AT4G19230	ABA
CYP707A2	AT2G29090	ABA
BG1	AT1G52400	ABA
NCED2	AT4G18350	ABA
LOX2	AT3G45140	JA
LOX3	AT1G17420	JA
AOS/DDE2	AT5G42650	JA
AOC1	AT3G25760	JA
AOC2	AT3G25770	JA
AOC3	AT3G25780	JA
OPCL1	AT1G20510	JA
ACX1	AT4G16760	JA
ACX4	AT3G51840	JA
JMT	AT1G19640	JA
JAR1	AT2G46370	JA
AtST2a	AT5G07010	JA
BBX1	AT5G15840	BBX
BBX2	AT5G15850	BBX
BBX3	AT3G02380	BBX
BBX5	AT5G24930	BBX
BBX6	AT5G57660	BBX
BBX8	AT5G48250	BBX
BBX11	AT2G47890	BBX
BBX14	AT1G68520	BBX
BBX15	AT1G25440	BBX
BBX16	AT1G73870	BBX
BBX17	AT1G49130	BBX
BBX22	AT1G78600	BBX
BBX27	AT1G68190	BBX
BBX28	AT4G27310	BBX
BBX29	AT5G54470	BBX
BBX30	AT4G15248	BBX
BBX31	AT3G21890	BBX
BBX32	AT3G21150	BBX
SAUR1	AT4G34770	SAUR
SAUR3	AT4G34790	SAUR
SAUR4	AT4G34800	SAUR
SAUR5	AT4G34810	SAUR
SAUR6	AT2G21210	SAUR
SAUR7	AT2G21200	SAUR
SAUR9	AT4G36110	SAUR
SAUR12	AT2G21220	SAUR
SAUR14	AT4G38840	SAUR
SAUR15	AT4G38850	SAUR
SAUR16	AT4G38860	SAUR
SAUR20	AT5G18020	SAUR
SAUR22	AT5G18050	SAUR
SAUR23	AT5G18060	SAUR
SAUR24	AT5G18080	SAUR
SAUR26	AT3G03850	SAUR
SAUR27	AT3G03840	SAUR
SAUR28	AT3G03830	SAUR
SAUR29	AT3G03820	SAUR
SAUR33	AT3G61900	SAUR
SAUR34	AT4G22620	SAUR
SAUR35	AT4G12410	SAUR
SAUR36	AT2G45210	SAUR
SAUR37	AT4G31320	SAUR
SAUR38	AT2G24400	SAUR
SAUR41	AT1G16510	SAUR
SAUR42	AT2G28085	SAUR
SAUR45	AT2G36210	SAUR
SAUR48	AT3G09870	SAUR
SAUR50	AT4G34760	SAUR
SAUR51	AT1G75580	SAUR
SAUR56	AT1G76190	SAUR
SAUR57	AT3G53250	SAUR
SAUR62	AT1G29430	SAUR
SAUR63	AT1G29440	SAUR
SAUR64	AT1G29450	SAUR
SAUR65	AT1G29460	SAUR
SAUR66	AT1G29500	SAUR
SAUR67	AT1G29510	SAUR
SAUR69	AT5G10990	SAUR
SAUR71	AT1G56150	SAUR
SAUR76	AT5G20820	SAUR
SAUR77	AT1G17345	SAUR
SAUR78	AT1G72430	SAUR
SAUR79	AT2G35290	SAUR
SAUR80	AT5G18030	SAUR
HSFA2	AT2G26150	HEAT
HSP70	AT3G12580	HEAT
HSP21	AT4G27670	HEAT
HSP17.6A	AT1G59860	HEAT
HSP17.6B	AT2G29500	HEAT
HSP18.2	AT5G59720	HEAT
TIL1	AT5G58070	light